import React, {Suspense} from 'react';
import Loader from './Loader';

export const LazyComponent = Component => {
  return props => (
    <Suspense fallback={<Loader />}>
      <Component {...props} />
    </Suspense>
  );
};
