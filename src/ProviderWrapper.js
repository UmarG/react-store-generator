import React, {Component} from 'react';
import Firebase, {Context} from './components/Firebase';

class ProviderWrapper extends Component {
  state = {
    firebase: new Firebase(),
    authenticated: false,
    user: null
  };
  render() {
    return (
      <Context.Provider
        value={{
          state: this.state,
          handleLogin: () => {
            this.setState({
              authenticated: true
            });
          },
          handleLogout: () => {
            this.state.firebase.doSignOut();
            this.setState({
              authenticated: false
            });
          }
        }}
      >
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default ProviderWrapper;
