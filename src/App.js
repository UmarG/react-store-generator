import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { LazyComponent } from './LazyComponent';
import { compose } from 'recompose';
import Navbar from './Navbar';
import PrivateRoute from './components/Dashboard/PrivateRoute';
import { withFirebase } from './components/Firebase';
import { withStyles } from '@material-ui/core';

const Dashboard = React.lazy(() => import('./components/Dashboard'));
const About = React.lazy(() => import('./components/Views/About'));
const Goal = React.lazy(() => import('./components/Views/Goal'));
const Contact = React.lazy(() => import('./components/Views/Contact'));
const Home = React.lazy(() => import('./Home'));
const Login = React.lazy(() => import('./Login'));
const Register = React.lazy(() => import('./Register'));

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex'
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  menuButtonHidden: {
    display: 'none'
  },
  title: {
    flexGrow: 1
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9
    }
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto'
  },
  chartContainer: {
    marginLeft: -22
  },
  tableContainer: {
    height: 320
  },
  h5: {
    marginBottom: theme.spacing.unit * 2
  }
});

const App = props => {
  const { authenticated, handleLogin, handleLogout, classes } = props;
  return (
    <div className="App">
      <BrowserRouter>
        <div className={classes.root}>
          <Navbar
            login={handleLogin}
            logout={handleLogout}
            authenticated={authenticated}
          />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />

            <Switch>
              <Route exact path="/" component={LazyComponent(Home)} />
              <Route path="/login" component={LazyComponent(Login)} />
              <Route path="/register" component={LazyComponent(Register)} />
              <Route path="/about" component={LazyComponent(About)} />
              <Route path="/contact" component={LazyComponent(Contact)} />
              <Route path="/our-goal" component={LazyComponent(Goal)} />
              <PrivateRoute
                authenticated={authenticated}
                path="/dashboard"
                component={LazyComponent(Dashboard)}
              />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default compose(
  withStyles(styles),
  withFirebase
)(App);
