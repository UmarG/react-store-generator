import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

const RegisterForm = props => {
  const {
    initialValues: {name, email, password, confirmPassword, error},
    errors,
    touched,
    handleSubmit,
    handleChange,
    isValid,
    setFieldTouched,
    classes
  } = props;

  const change = (name, e) => {
    e.persist();
    handleChange(e);
    setFieldTouched(name, true, false);
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <TextField
        id="name"
        name="name"
        type="text"
        helperText={touched.name ? errors.name : ''}
        error={touched.name && Boolean(errors.name)}
        value={name}
        label="Name"
        margin="normal"
        required
        onChange={change.bind(null, 'name')}
        fullWidth
      />
      <TextField
        id="email"
        name="email"
        helperText={touched.email ? errors.email : ''}
        error={touched.email && Boolean(errors.email)}
        label="Email"
        margin="normal"
        type="email"
        required
        fullWidth
        value={email}
        onChange={change.bind(null, 'email')}
      />
      <TextField
        id="password"
        margin="normal"
        required
        name="password"
        helperText={touched.password ? errors.password : ''}
        error={touched.password && Boolean(errors.password)}
        label="Password"
        fullWidth
        type="password"
        value={password}
        onChange={change.bind(null, 'password')}
      />
      <TextField
        id="confirmPassword"
        name="confirmPassword"
        helperText={touched.confirmPassword ? errors.confirmPassword : ''}
        error={touched.confirmPassword && Boolean(errors.confirmPassword)}
        label="Confirm Password"
        fullWidth
        required
        margin="normal"
        type="password"
        value={confirmPassword}
        onChange={change.bind(null, 'confirmPassword')}
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
        disabled={!isValid}
      >
        Register
      </Button>
      {error && <p>{error.message}</p>}
    </form>
  );
};

export default withStyles(styles)(RegisterForm);
