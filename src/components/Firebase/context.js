import React from 'react';

const Context = React.createContext(null);

export const withFirebase = Component => props => (
  <Context.Consumer>
    {context => (
      <Component
        {...props}
        handleLogin={context.handleLogin}
        handleLogout={context.handleLogout}
        authenticated={context.state.authenticated}
        firebase={context.state.firebase}
      />
    )}
  </Context.Consumer>
);

export default Context;
