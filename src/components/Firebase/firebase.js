import app from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyBLo8BsK_NdAJxhBOJUcj50wqjynet033k',
  authDomain: 'storefront-generator.firebaseapp.com',
  databaseURL: 'https://storefront-generator.firebaseio.com',
  projectId: 'storefront-generator',
  storageBucket: '',
  messagingSenderId: '227990525095'
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
  }
  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = password => this.auth.currentUser.updatePassword(password);
}

export default Firebase;
