import Context, {withFirebase} from './context';
import Firebase from './firebase';

export default Firebase;

export {Context, withFirebase};
