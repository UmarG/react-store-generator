work in progress.

Stack:

- React
- Material UI
- Firebase
- Storefront API

V1 Todo:

- Setup Heroku
- Handle Sessions
- Animate the Routes ✅
- Add Validation ✅
- Sync User Creds to Shopify Creds

- Create Dashboard View ✅
- Create Account Views (Settings etc)
- Create Theme Editor
